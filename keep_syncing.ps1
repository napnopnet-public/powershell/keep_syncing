﻿<#
.SYNOPSIS
Continuously mirror a directory.  
.Description
Simple synchronization script consisting of while loop, sleep interval and a robocopy command. 
#> 

param ($catch, [switch]$help, $source_dir, $destination_dir, [string]$excluded_dirs, [string]$excluded_files, [string] $flags, [int] $interval)


$current_script_path = ''

$usage_string = "
    
    Try 
       $ $PSCommandPath -help 
       $
    for alternative help

    usage: .\keep_syncing.ps1 [-source_dir <path>] [-destination_dir <path>] [[-excluded_dirs <paths>]]
                              [[-excluded_files <paths>]] [[-flags <flags>]] [[-interval <seconds>]]

    Parameter between single brackets, [], are needed and will be promted for if not supplied, parameters between doouble brackets,  [[]], are not. 

    -source_dir
                :: <path> is a string containing a path: use double quotes around a path containing spaces.
                -- The directory that must be mirrored to the destination directory

    -destination_dir
                :: <path> is a string containing a path: use double quotes around a paths containing spaces.
                -- The directory where to the source directory is mirrored
                     
    -excluded_dirs
                :: <paths> is a string between double quotes, containing paths separated by pipe character `"|`". 
                -- The directories that must not be synchronized. 

    -excluded_files
                :: <path> is a string between double quotes, containing paths separated by pipe character `"|`".
                -- The files that must not be synchronized. 

    -flags
                :: <flags> is a string between single quotes, containing robocopy 'options'.
                -- Do `"$ robocopy /?`"on the command line for info about options. Defaults to '/MIR /MT'.


    -interval 
                :: <seconds> is a integer. 
                -- The number of seconds between repeated synchonization events
            

"



if($help -eq $true){
  $usage_string
  exit
}

if($catch -eq 'help' -or $catch -eq '/?' -or $catch -eq '?'){
    "  Try 
    $PSCommandPath -help
  or 
    get-help $PSCommandPath"
    exit
}

if($catch -ne $null -or ($catch -eq $null -and $PSBoundParameters.Values.Count -eq 0 )){
"  Try 
    $PSCommandPath -help
  or 
    get-help $PSCommandPath"

}

"



Review settings: Crt-c to abord, Enter to start...


"


###
# $excluded_dirs
##
function Handle-Input-Excluded-Dirs { #https://ss64.com/ps/syntax-functions.html
   $script:excluded_dirs = read-host -Prompt "Give paths of excluded Directories, separated by a `"|`", pipe character"
}
if($excluded_dirs -eq $null -or $excluded_dirs -eq ''){
    Handle-Input-Excluded-Dirs
}
$resolved_excluded_dirs_string = ''



$resolved_exluded_dirs_string = ''
if($excluded_dirs -ne $null){
    if($excluded_dirs -ne ''){

        $excluded_dirs = $excluded_dirs.replace('"','')
        $excluded_dirs_array = $excluded_dirs.split("|")

        for ($i=1;$i -le $excluded_dirs_array.length; $i++) {

            Try
            {
               
               $pathIsOk = Resolve-Path -Path  $excluded_dirs_array[$i-1]   -ErrorAction Stop
               
               if($i -eq $excluded_dirs_array.length ){
                    $resolved_exluded_dirs_string += '"' + $pathIsOk + '"'

               }else{
                $resolved_exluded_dirs_string += '"' + $pathIsOk + '", '
               }
      
            }
            Catch
            {
              Write-Host "One of excluded directories: " $_.Exception.Message
              exit
            }
   
        }
    }
}
if($resolved_exluded_dirs_string -ne ''){
    $resolved_exluded_dirs_string = ' /XD ' + $resolved_exluded_dirs_string
}
"Excluded directories: $resolved_exluded_dirs_string"


###
# $excluded_files
##
function Handle-Input-Excluded-Files { #https://ss64.com/ps/syntax-functions.html
   $script:excluded_files = read-host -Prompt "Give paths of excluded files, separated by a `"|`", pipe character"
}
if($excluded_files -eq $null -or $excluded_files -eq ''){
    Handle-Input-Excluded-Files
}
$resolved_excluded_files_string = ''
if($excluded_files -ne $null){
    if($excluded_files -ne ''){
       
        $excluded_files = $excluded_files.replace('"','')
        $excluded_files_array = $excluded_files.split("|")

        #$resolved_excluded_files_string = ''

        for ($i=1;$i -le $excluded_files_array.length; $i++) {

            Try
            {
               $pathIsOk = Resolve-Path -Path  $excluded_files_array[$i-1]   -ErrorAction Stop

               if($i -eq $excluded_files_array.length ){
                    $resolved_excluded_files_string += '"' + $pathIsOk + '"'

               }else{
                $resolved_excluded_files_string += '"' + $pathIsOk + '", '
               }
      
            }
            Catch
            {
              Write-Host "One of excluded files: " $_.Exception.Message
              exit
            }

        }
    }
}
if($resolved_excluded_files_string -ne ''){
    $resolved_excluded_files_string = ' /XF ' + $resolved_excluded_files_string
}

"Excluded files: $resolved_excluded_files_string"


###
# $interval
##
function Handle-Input-Interval { #https://ss64.com/ps/syntax-functions.html
   $script:interval = read-host -Prompt "For seconds interval: please enter a integer number, defaults to '1' (== 1 second)"
}
if($interval -eq $null -or $interval -eq ''){
   Handle-Input-Interval
}
if($interval -eq ''){
   $interval = 1
}

"Time between mirror events: $interval seconds"


###
# $flags
##
function Handle-Input-Flags { #https://ss64.com/ps/syntax-functions.html
   $script:flags = read-host -Prompt "Enter robocopy options, defaults to '/MIR /MT' (see command 'robocopy /? ) "
}
if($flags -eq $null -or $flags -eq ''){
    Handle-Input-Flags
}
if($flags -eq ''){
   $flags = '/MIR /MT'
}
"Robocopy options used: $flags"

###
# $source_dir
##
function Handle-Input-Source_dir { #https://ss64.com/ps/syntax-functions.html
   $script:source_dir = read-host -Prompt "Please enter a source directory, defaults to '.' (==current directory)"

}

if ($source_dir -eq $null -or $source_dir -eq ''){
   
   Handle-Input-Source_dir
   if($source_dir -eq '') {
    $source_dir = "."
   }
   
}

Try
{
   $source_dir = $source_dir.Replace('"','')
   $source_dir= Resolve-Path -Path  $source_dir   -ErrorAction Stop
}
Catch
{
  Write-Host $_.Exception.Message
  exit
}
"Directory $source_dir will be mirrored"

###
# $destination_dir
##

function Handle-Input-Destination_dir { #https://ss64.com/ps/syntax-functions.html
   $script:destination_dir = read-host -Prompt "Please enter destination directory path"

}

if ($destination_dir -eq $null  -or $destination_dir -eq ''){
   Handle-Input-Destination_dir
   
}

Try
{
    $destination_dir =  $destination_dir.replace('"','')
    $destination_dir = Resolve-Path -Path $destination_dir  -ErrorAction Stop
    $pattern = '^' +  [Regex]::Escape($source_dir) # https://stackoverflow.com/questions/23651862/powershell-how-to-escape-all-regex-characters-from-a-string
    if($destination_dir -match $pattern){
        "Distination directory inside source directory: For now this is not allowed"
        exit

    }
}
Catch
{
    Write-Host $_.Exception.Message
    exit
}
"The directory where $source_dir is mirrored to: $destination_dir"


$review_ready = read-host -Prompt "Review setting: Ctr-c to abort, enter to start"
$review_ready
if($review_ready -eq ''){
    
}
 
Do
{

   Write-Host "waiting $interval seconds before repeat..."
   #Start-Sleep -Milliseconds $interval
   Start-Sleep -Seconds $interval

   

   # see https://stackoverflow.com/questions/6338015/how-do-you-execute-an-arbitrary-native-command-from-a-string

  $rocomand = 'robocopy "' + $source_dir + '" "' + $destination_dir + '" ' +  $resolved_excluded_files_string + $resolved_exluded_dirs_string +' ' + $flags +''
 
  $rocomand
  Invoke-Expression -Command $rocomand

   #Write-Host "waiting $interval seconds before repeat..."
   ##Start-Sleep -Milliseconds $interval
   #Start-Sleep -Seconds $interval


} while ($true)

## https://www.red-gate.com/simple-talk/sysadmin/powershell/how-to-add-help-to-powershell-scripts/
## see https://www.rainingforks.com/blog/2015/suggested-robocopy-switches-explained.html
## see https://www.red-gate.com/simple-talk/sysadmin/powershell/how-to-use-parameters-in-powershell/
## https://stackoverflow.com/questions/25869806/how-to-keep-2-folders-in-sync-using-powershell-script/25870879
## https://www.vexasoft.com/blogs/powershell/7255220-powershell-tutorial-try-catch-finally-and-error-handling-in-powershell